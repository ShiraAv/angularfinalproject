import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import {AF} from "./providers/af";
import {RouterModule, Routes} from "@angular/router";

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RegistrationPageComponent } from './registration-page/registration-page.component';

export const firebaseConfig = {
    apiKey: "AIzaSyDmmTOb1MCFcCtWaPB5MJ4Bi0NZFQMFvDs",
    authDomain: "angularchat-76432.firebaseapp.com",
    databaseURL: "https://angularchat-76432.firebaseio.com",
    projectId: "angularchat-76432",
    storageBucket: "angularchat-76432.appspot.com",
    messagingSenderId: "831164717864"
 };

 const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegistrationPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    LoginPageComponent,
    HomePageComponent,
    RegistrationPageComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(routes)

  ],
  providers: [AF],
  bootstrap: [AppComponent]
})
export class AppModule { }
